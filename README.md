# @yepreally/eslint-config

ESLint rule configuration based on work from [eslint-plugin-bmcallis](https://gitlab.com/bmcallis/eslint-config-bmcallis).

http://eslint.org/docs/rules/

Plugins Used:
- [eslint-plugin-import](https://github.com/benmosher/eslint-plugin-import)
- [eslint-plugin-jest](https://github.com/jest-community/eslint-plugin-jest)
- [eslint-plugin-node](https://github.com/mysticatea/eslint-plugin-node)
- [eslint-plugin-react](https://github.com/yannickcr/eslint-plugin-react)
- [eslint-plugin-react-hooks](https://github.com/facebook/react/tree/main/packages/eslint-plugin-react-hooks)

## Usage

Install the configs by running:

```
npm install --save-dev eslint @yepreally/eslint-config
```

Note that the plugins and parser used are dependencies of this project and will also be installed to your project, so you don't need to specify them individually.

Then add the extends to your `.eslintrc.js`:

```javascript
module.exports = {
  extends: ['@yepreally/eslint-config'],
  rules: {
    // your overrides
  }
}
```

## Smaller Bits

By extending `@yepreally/eslint-config` you're getting all of the configs for eslint, import, jest, react, and typescript. If you only want some of these you can extend them individually.

```javascript
module.exports = {
  extends: [
   '@yepreally/eslint-config/core',
   '@yepreally/eslint-config/import',
   '@yepreally/eslint-config/jest',
   '@yepreally/eslint-config/react',
   '@yepreally/eslint-config/typescript',
  ],
  rules: {
    // overrides
  }
}
```

