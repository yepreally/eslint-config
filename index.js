module.exports = {
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  parserOptions: {
    ecmaVersion: 9,
    sourceType: 'module',
  },
  extends: [
    './core.js',
    './import.js',
    './jest.js',
    './react.js',
    './typescript.js',
  ],
};
